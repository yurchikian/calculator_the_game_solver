class CalculatorAssembler
  def self.assemble definition
    calc = Calculator.new definition[:initial_value]
  
    definition[:buttons].each do |button|
      calc.add_button(button[0], button[1])
    end

    calc
  end
end