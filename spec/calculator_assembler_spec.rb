RSpec.describe CalculatorAssembler do
  it 'assembles successfully' do
    test_level = {
      initial_value: 552,
      buttons: [
        [DivButton, 5],
        [SumButton, nil],
        [MultButton, 8],
        [SubButton, [12, 15]]
      ]
    }

    calculator = CalculatorAssembler.assemble test_level

    calculator2 = Calculator.new 552

    calculator2.add_button(DivButton, 5)
    calculator2.add_button(SumButton, nil)
    calculator2.add_button(MultButton, 8)
    calculator2.add_button(SubButton, [12, 15])

    calc_buttons = calculator.buttons.map { |b| [b.class, b.value] }
    calc2_buttons = calculator2.buttons.map { |b| [b.class, b.value] }

    expect(calc_buttons).to eq(calc2_buttons)
  end
end
