RSpec.describe DivButton do
  it 'changes current value of the calculator' do
    calculator = Calculator.new 8
    calculator.add_button(DivButton, 2)

    calculator.buttons.last.click
    expect(calculator.current_value).to eq(4)
  end
end
