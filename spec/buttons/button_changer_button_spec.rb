RSpec.describe ButtonChangerButton do
  calculator = Calculator.new 200
  calculator.add_button(PlusButton, 1)
  calculator.add_button(ShiftButton, 'left')
  calculator.add_button(ButtonChangerButton, 1)

  it 'changes values of the buttons on the calculator' do
    calculator.buttons.last.click
    expect(calculator.buttons.first.value).to eq(2)
  end

  it 'does not changes it\'s own value' do
    expect(calculator.buttons.last.value).to eq(1)
  end

  it 'does not touch non-numeric buttons' do
    expect(calculator.buttons[1].value).to eq('left')
  end

  it 'can also decrease values' do
    calculator.add_button(ButtonChangerButton, -2)
    calculator.buttons.last.click
    expect(calculator.buttons.first.value).to eq(0)
  end
end
