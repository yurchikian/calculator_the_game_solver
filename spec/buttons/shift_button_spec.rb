RSpec.describe ShiftButton do
  context 'changes current value of the calculator' do
    calculator = Calculator.new 5603

    it 'rotates left' do
      calculator.add_button(ShiftButton, 'left')

      calculator.buttons.last.click
      expect(calculator.current_value).to eq(6035)
    end

    it 'rotates right' do
      calculator.add_button(ShiftButton, 'right')

      calculator.buttons.last.click
      calculator.buttons.last.click
      expect(calculator.current_value).to eq(3560)
    end

    it 'removes leading zeros' do
      calculator.buttons.last.click
      expect(calculator.current_value).to eq(356)
    end
  end

  context 'works properly with negative values' do
    calculator = Calculator.new 151
    calculator.add_button(NegateButton, nil)
    calculator.add_button(ShiftButton, 'left')

    it 'can shift negative value' do
      calculator.buttons.first.click
      calculator.buttons.last.click

      expect(calculator.current_value).to eq(-511)
    end
  end
end
