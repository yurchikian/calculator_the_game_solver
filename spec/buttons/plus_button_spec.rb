RSpec.describe PlusButton do
  it 'changes current value of the calculator' do
    calculator = Calculator.new 5
    calculator.add_button(PlusButton, 2)

    calculator.buttons.last.click
    expect(calculator.current_value).to eq(7)
  end
end
