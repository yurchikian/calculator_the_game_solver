RSpec.describe SumButton do
  it 'changes current value of the calculator' do
    calculator = Calculator.new 71
    calculator.add_button(SumButton, nil)

    calculator.buttons.last.click
    expect(calculator.current_value).to eq(8)
  end
end
