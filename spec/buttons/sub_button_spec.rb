RSpec.describe SubButton do
  it 'changes current value of the calculator' do
    calculator = Calculator.new 1212
    calculator.add_button(SubButton, [21, 0])

    calculator.buttons.last.click
    expect(calculator.current_value).to eq(102)
  end
end
