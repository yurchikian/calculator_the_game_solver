RSpec.describe ReverseButton do
  context 'changes current value of the calculator' do
    calculator = Calculator.new 5821
    calculator.add_button(ReverseButton, nil)

    it 'works with positive values' do
      calculator.buttons.last.click
      expect(calculator.current_value).to eq(1285)
    end

    it 'works with negative values' do
      calculator.add_button(NegateButton, nil)
      calculator.buttons.last.click
      calculator.buttons.first.click
      expect(calculator.current_value).to eq(-5821)
    end
  end
end
