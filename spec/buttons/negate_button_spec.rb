RSpec.describe NegateButton do
  context 'changes current value of the calculator' do
    calculator = Calculator.new 150
    calculator.add_button(NegateButton, nil)

    it 'makes positive values negative' do

      calculator.buttons.last.click
      expect(calculator.current_value).to eq(-150)
    end

    it 'makes negative values positive' do
      calculator.buttons.last.click
      expect(calculator.current_value).to eq(150)
    end
  end
end
