RSpec.describe MinusButton do
  it 'changes current value of the calculator' do
    calculator = Calculator.new 5
    calculator.add_button(MinusButton, 2)

    calculator.buttons.last.click
    expect(calculator.current_value).to eq(3)
  end
end
