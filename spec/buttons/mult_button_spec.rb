RSpec.describe MultButton do
  it 'changes current value of the calculator' do
    calculator = Calculator.new 5
    calculator.add_button(MultButton, 2)

    calculator.buttons.last.click
    expect(calculator.current_value).to eq(10)
  end
end
