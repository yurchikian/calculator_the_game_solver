RSpec.describe ValueButton do
  it 'changes current value of the calculator' do
    calculator = Calculator.new 31
    calculator.add_button(ValueButton, 2)

    calculator.buttons.last.click
    expect(calculator.current_value).to eq(312)
  end
end
