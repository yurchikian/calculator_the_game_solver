RSpec.describe MirrorButton do
  context 'changes current value of the calculator' do
    calculator = Calculator.new 123
    calculator.add_button(MirrorButton, nil)

    it 'works with positive values' do
      calculator.buttons.last.click
      expect(calculator.current_value).to eq(123_321)
    end

    it 'works with negative values' do
      calculator.current_value = -204
      calculator.buttons.last.click
      expect(calculator.current_value).to eq(-204_402)
    end
  end
end
