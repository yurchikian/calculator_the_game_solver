RSpec.describe Inv10Button do
  context 'changes current value of the calculator' do
    calculator = Calculator.new 127
    calculator.add_button(Inv10Button, nil)

    it 'works with positive values' do
      calculator.buttons.last.click
      expect(calculator.current_value).to eq(983)
    end

    it 'works with negative values' do
      calculator.current_value = -214
      calculator.buttons.last.click
      expect(calculator.current_value).to eq(-896)
    end

    it 'properly handles the zero' do
      calculator.current_value = 1009
      calculator.buttons.last.click
      expect(calculator.current_value).to eq(9001)
    end
  end
end
