RSpec.describe Calculator do

  let(:calc) {
    calc = Calculator.new 0

    calc.add_button(PlusButton, 6)
    calc.add_button(SubButton, [6, 7])

    calc
  }

  it 'adds buttons' do
    expect(calc.buttons.first.class).to be(PlusButton)
    expect(calc.buttons.first.value).to eq(6)
    expect(calc.buttons.count).to eq(2)
  end

  it 'can be clicked and can be reset to inital value' do
    calc.buttons.first.click
    expect(calc.current_value).to eq(6)

    calc.reset
    expect(calc.current_value).to eq(0)
  end

  it 'can be solved' do
    solutions = calc.find_solution(7, 2)
    expect(solutions.count).to eq(1)
    
    solution = solutions[0]
    expect(solution[0].class).to be(PlusButton)
    expect(solution[1].class).to be(SubButton)
  end

  it 'can be non-solved' do
    solutions = calc.find_solution(7, 1)
    expect(solutions).to be_empty

    solutions = calc.find_solution(3, 3)
    expect(solutions).to be_empty
  end
end
