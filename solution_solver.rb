class SolutionSolver
  def self.solve_level(level)
    definition = load_level(level)
    calc = Calculator.new definition['initial_value']

    definition['buttons'].each do |button|
      calc.add_button(Kernel.const_get(button[0]), button[1])
    end

    calc.find_solution(definition['goal'], definition['number_of_moves']).map do |solution|
      pretty_buttonlist(solution)
    end
  end

  def self.load_level(level)
    YAML.safe_load(File.open('./levels/levels.yml'))[level]
  end

  def self.pretty_buttonlist(buttons)
    calc.initial_value.to_s + buttons.map{ |button| "[#{button}]" }.join(' -> ')
  end
end
