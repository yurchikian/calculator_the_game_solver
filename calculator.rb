class Calculator

  attr_accessor :initial_value, :current_value, :buttons

  def initialize(initial_value)
    @initial_value = initial_value
    @buttons = []
    reset
  end

  def add_button(button, value)
    @buttons << button.new(value, self)
  end

  def reset
    @current_value = @initial_value
  end

  def find_solution(goal, number_of_moves)
    permutations = @buttons.repeated_permutation(number_of_moves).to_a

    permutations.select! do |perm|
      reset
      perm.each(&:click)
      @current_value == goal
    end

    permutations
  end
end
