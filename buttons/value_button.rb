class ValueButton < Button
  def click
    @calculator.current_value = (@calculator.current_value.to_s + @value.to_s).to_i
  end

  def to_s
    @value.to_s
  end
end
