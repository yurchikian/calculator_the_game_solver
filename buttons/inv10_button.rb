class Inv10Button < Button
  def click
    @calculator.current_value = @calculator.current_value.to_s.chars.map do |number|
      if ('1'..'9').cover? number
        10 - number.to_i
      else
        number
      end
    end.join.to_i
  end

  def to_s
    'Inv10'
  end
end
