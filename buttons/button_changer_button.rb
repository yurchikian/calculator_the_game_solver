class ButtonChangerButton < Button
  def click
    @calculator.buttons.each do |button|
      next if button == self
      next if button.value.class != Integer
      button.value += @value
    end
  end

  def to_s
    "[#{value.negative? ? '-' : '+'}] #{value.abs}"
  end
end
