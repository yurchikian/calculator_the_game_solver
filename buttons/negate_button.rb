class NegateButton < Button
  def click
    @calculator.current_value = -@calculator.current_value
  end

  def to_s
    "+/-"
  end
end
