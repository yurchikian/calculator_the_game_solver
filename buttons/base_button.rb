class Button
  attr_accessor :value

  def initialize(value, calculator)
    @value = value
    @calculator = calculator
  end

  def click
    raise NotImplementedError
  end

  def to_s
    'base_button'
  end
end
