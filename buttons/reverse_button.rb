class ReverseButton < Button
  def click
    negative = @calculator.current_value.negative?
    value = @calculator.current_value.to_s.chars
    value.shift if negative

    @calculator.current_value = ((negative ? '-' : '') + (value.reverse).join).to_i
  end

  def to_s
    'Reverse'
  end
end
