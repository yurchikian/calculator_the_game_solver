class SumButton < Button
  def click
    @calculator.current_value = @calculator.current_value.to_s.chars.map(&:to_i).inject(:+)
  end

  def to_s
    'SUM'
  end
end
