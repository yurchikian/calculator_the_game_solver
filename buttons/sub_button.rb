class SubButton < Button
  def click
    current_value = @calculator.current_value.to_s
    @calculator.current_value = current_value.gsub(@value[0].to_s, @value[1].to_s).to_i
  end

  def to_s
    "#{@value[0]} => #{@value[1]}"
  end
end
