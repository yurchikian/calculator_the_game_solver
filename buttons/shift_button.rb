class ShiftButton < Button
  def click
    direction = @value == 'left' ? 1 : -1
    negative = @calculator.current_value.negative?
    value = @calculator.current_value.to_s.chars
    value.shift if negative

    @calculator.current_value = ((negative ? '-' : '') + value.rotate(direction).join).to_i
  end

  def to_s
    @value == 'left' ? '< Shift' : 'Shift >'
  end
end
