class MinusButton < Button
  def click
    @calculator.current_value -= @value
  end

  def to_s
    "-#{@value}"
  end
end