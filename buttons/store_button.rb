class StoreButton < Button
  def initialize(_, _)
    raise NotImplementedError
  end

  def click
    @calculator.current_value # @value
  end

  def to_s
    'Store'
  end
end
